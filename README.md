# Question 5

## Live-Test

You can test the query on the following link [https://sqliteonline.com/](https://sqliteonline.com/) by copy-pasting the following commands.  

CREATE TABLE Students (ID integer PRIMARY KEY AUTOINCREMENT, name varchar(50), Value integer);  
CREATE TABLE Notes (Grade integer PRIMARY KEY AUTOINCREMENT, Min_Value integer, Max_Value integer);  

INSERT INTO Notes  
	(Grade, Min_Value, Max_Value)  
VALUES  
	(1,0,9),  
	(2,10,19),  
	(3,20,29),  
	(4,30,39),  
	(5,40,49),  
	(6,50,59),  
	(7,60,69),  
	(8,70,79),  
	(9,80,89),  
	(10,90,100);  

INSERT INTO Students  
	(ID, name, Value)  
VALUES  
	(1, "Julia", 81),  
	(2, "Carol", 68),  
	(3, "Maria", 99),  
	(4, "Andreia", 78),  
	(5, "Jaqueline", 63),  
	(6, "Marcela", 88);  

SELECT * FROM (SELECT S.Name as Name, N.Grade as Grade, S.value as Value FROM Students S inner join Notes N ON (FLOOR(S.value / 10) * 10) = N.min_value WHERE N.grade >= 8 ORDER BY N.Grade Desc, S.Value Desc, S.Name ASC)  
UNION ALL  
SELECT * FROM (SELECT "NULL" as Name, N.Grade as Grade, S.value as Value FROM Students S inner join Notes N ON (FLOOR(S.value / 10) * 10) = N.min_value WHERE N.grade < 8 ORDER BY N.Grade Asc, S.Value Asc);  

## Comments
The query is split in two sections, the first one gets all students that got a grade bigger or equal to 8 and orders them, and the second section gets the students that graded below 8, orders them and changes their name to "NULL".
