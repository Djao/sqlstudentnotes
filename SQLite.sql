SELECT * FROM (SELECT S.Name as Name, N.Grade as Grade, S.value as Value FROM Students S inner join Notes N ON (FLOOR(S.value / 10) * 10) = N.min_value WHERE N.grade >= 8 ORDER BY N.Grade Desc, S.Value Desc, S.Name ASC)
UNION ALL
SELECT * FROM (SELECT "NULL" as Name, N.Grade as Grade, S.value as Value FROM Students S inner join Notes N ON (FLOOR(S.value / 10) * 10) = N.min_value WHERE N.grade < 8 ORDER BY N.Grade Asc, S.Value Asc);
